# EASY

# Return the argument with all its lowercase characters removed.
def destructive_uppercase(str)
  str.delete!("a-z")
end

# Return the middle character of a string. Return the middle two characters if
# the word is of even length, e.g. middle_substring("middle") => "dd",
# middle_substring("mid") => "i"
def middle_substring(str)
  str.length.odd? ? str[str.length/2] : str[str.length/2-1] + str[str.length/2]
end

# Return the number of vowels in a string.
VOWELS = %w(a e i o u)
def num_vowels(str)
  str.chars.count{|s|VOWELS.include?(s.downcase)}
end

# Return the factoral of the argument (num). A number's factorial is the product
# of all whole numbers between 1 and the number itself. Assume the argument will
# be > 0.
def factorial(num)
  return nil if num < 0

  result = 1
  while num > 0
    result = result * num
    num -= 1
  end

  return result
end


# MEDIUM

# Write your own version of the join method. separator = "" ensures that the
# default seperator is an empty string.
def my_join(arr, separator="")
  str = ""
  arr.each.with_index do |x,i|
    i == (arr.length-1) ? str << x : str << x << separator
  end
  str
end

# Write a method that converts its argument to weirdcase, where every odd
# character is lowercase and every even is uppercase, e.g.
# weirdcase("weirdcase") => "wEiRdCaSe"
def weirdcase(str)
  str.chars.map.with_index{|s,i| i.even? ? s.downcase : s.upcase}.join('')
end

# Reverse all words of five more more letters in a string. Return the resulting
# string, e.g., reverse_five("Looks like my luck has reversed") => "skooL like
# my luck has desrever")
def reverse_five(str)
  str.split(' ').map{|s| s.length >= 5 ? s.reverse : s }.join(' ')
end

# Return an array of integers from 1 to n (inclusive), except for each multiple
# of 3 replace the integer with "fizz", for each multiple of 5 replace the
# integer with "buzz", and for each multiple of both 3 and 5, replace the
# integer with "fizzbuzz".
def fizzbuzz(n)
  (1..n).to_a.map do |x|
    if (x % 3 == 0) && (x % 5 == 0)
      "fizzbuzz"
    elsif x % 5 == 0
      "buzz"
    elsif x % 3 == 0
      "fizz"
    else
      x
    end
  end
end


# HARD

# Write a method that returns a new array containing all the elements of the
# original array in reverse order.
def my_reverse(arr)
  arr.reverse
end

# Write a method that returns a boolean indicating whether the argument is
# prime.
def prime?(num)
  num <= 1 ? (return false) : (2...num).each{|x| return false if num % x == 0}
  return true
end

# Write a method that returns a sorted array of the factors of its argument.
def factors(num)
  (1..num).select{|x|num%x==0}
end

# Write a method that returns a sorted array of the prime factors of its argument.
def prime_factors(num)
  factors(num).select{|x|prime?(x)}.sort
end

# Write a method that returns the number of prime factors of its argument.
def num_prime_factors(num)
  prime_factors(num).length
end


# EXPERT

# Return the one integer in an array that is even or odd while the rest are of
# opposite parity, e.g. oddball([1,2,3]) => 2, oddball([2,4,5,6] => 5)
def oddball(arr)
  n_a = arr.map{|x|x.even?}
  n_a.count(true) > 1 ? arr[n_a.index(false)] : arr[n_a.index(true)]
end
